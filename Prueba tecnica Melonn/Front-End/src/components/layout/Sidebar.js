import React from 'react';
import NuevoPedido from '../pedidos/NuevoPedido'
import ListadoPedidos from '../pedidos/ListadoPedidos'
const Sidebar = () => {


    return ( 
        <aside>
            <h1>Melonn</h1>
            <NuevoPedido/>
            <div className = "pedidos">
                <h2>Tus pedidos</h2>
                <ListadoPedidos/>
            </div>
        </aside>
     );
}
 
export default Sidebar;