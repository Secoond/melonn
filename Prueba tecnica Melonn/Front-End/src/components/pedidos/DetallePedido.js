import React, {useEffect, useContext, useState} from 'react';
import Producto from './Producto'
import pedidoContext from '../../context/pedidos/pedidoContext'
import axios from 'axios'


const DetallePedido = () => {

    const pedidosContext = useContext(pedidoContext);
    const { pedido, actualizarMostrarPedido,  actualizarMostrarDetalle} = pedidosContext;

    //State para contener los metodos de envio.
    const [listaMetodosState, modificarListaMetodosState] = useState({metodos: []});

    const {metodos} = listaMetodosState;
    
    const obtenerNombreMetodo = id =>{
        for(let i = 0; i < metodos.length; i++){
            if(metodos[i].id === parseInt(id))
                return metodos[i].name;
        }
        return null;
    }

    const dateFormat = (fecha) =>
    {
        let ms = Date.parse(fecha);
        let fechaNew = new Date(ms);
        return fechaNew.toString();
    }

    const verResumenPedido = e =>{
        actualizarMostrarPedido(true);
        actualizarMostrarDetalle(false);
    }

    useEffect(() =>{
        if(metodos.length === 0){
            axios.get("http://localhost:4000/api/Pedidos/ListaMetodos", {
            }, {
            }).then(response => {
                modificarListaMetodosState({metodos: response.data.body});
            }).catch(function(err) {
                console.log(err);
            });
        }
    })
    return ( 
        <div className = "contenedor-detalle-pedido sombra-dark">
            <div className = "contenedor-form sombra-dark">
                <h2>Información del pedido</h2>
                <div className = "campo-form-detalle">
                    <label className = "titulo">Numero de orden de venta: </label>
                    <label>{pedido.nroPedido}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">Vendedor: </label>
                    <label>{pedido.vendedor}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">fecha de creacion: </label>
                    <label>{dateFormat(pedido.fechaCreacion)}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">Metodo de envío: </label>
                    <label>{obtenerNombreMetodo(pedido.metodoEnvio)}</label>
                </div>
            </div>
            <div className = "contenedor-form sombra-dark">
                <h2>Datos de envio</h2>
                <div className = "campo-form-detalle">
                    <label className = "titulo">Direccion de envío: </label>
                    <label>{pedido.direccionEnvio}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">Ciudad de envío: </label>
                    <label>{pedido.ciudadEnvio}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">Región de envío: </label>
                    <label>{pedido.regionEnvio}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">País de envío: </label>
                    <label>{pedido.paisEnvio}</label>
                </div>
            </div>
            <div className = "contenedor-form sombra-dark">
                <h2>Fecha de promesas</h2>
                <div className = "campo-form-detalle">
                    <label className = "titulo">pack_promise_min: </label>
                    <label>{pedido.pack_promise_min}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">pack_promise_max: </label>
                    <label>{pedido.pack_promise_max}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">ship_promise_min: </label>
                    <label>{pedido.ship_promise_min}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">ship_promise_max: </label>
                    <label>{pedido.ship_promise_max}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">delivery_promise_min: </label>
                    <label>{pedido.delivery_promise_min}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">delivery_promise_max: </label>
                    <label>{pedido.delivery_promise_max}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">ready_pickup_promise_min: </label>
                    <label>{pedido.ready_pickup_promise_min}</label>
                </div>
                <div className = "campo-form-detalle">
                    <label className = "titulo">ready_pickup_promise_max: </label>
                    <label>{pedido.ready_pickup_promise_max}</label>
                </div>
            </div>
            <div className = "contenedor-form sombra-dark">
                <h2>Lista de productos</h2>
                    {
                        pedido.listaProductos.map(producto =>(
                            <Producto 
                                key = {producto.id}
                                producto = {producto}
                                pedido =  {pedido}
                            />
                        ))
                    }
                    <button className = "btn btn-primario btn-block" onClick = {(e) => verResumenPedido(e)}>Volver</button>
            </div>
        </div>
     );
}
 
export default DetallePedido;