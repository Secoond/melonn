import React, {Fragment, useContext} from 'react';
import pedidoContext from '../../context/pedidos/pedidoContext'
const NuevoPedido = () => {

    //Extraer funcion de modificacion de formulario.
    const pedidosContext = useContext(pedidoContext);
    const { mostrarFormulario, mostrarErrorFormulario, actualizarMostrarDetalle, actualizarMostrarPedido } = pedidosContext;

    const visualizarFormulario = () =>{
        mostrarErrorFormulario(false);
        actualizarMostrarDetalle(false);
        actualizarMostrarPedido(false);
        mostrarFormulario(true);
    }
    return ( 
        <Fragment>
            <button
                type = "button"
                className = "btn btn-block btn-primario"
                onClick = {() => visualizarFormulario()}>
                    Nuevo Pedido</button>
        </Fragment>
        
     );
}
 
export default NuevoPedido;