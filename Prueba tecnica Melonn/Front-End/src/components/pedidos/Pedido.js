import React , {useContext} from 'react';
import pedidoContext from '../../context/pedidos/pedidoContext'
const Pedido = ({pedido}) => {
    //Extraemos los pedidos.
    const pedidosContext = useContext(pedidoContext);
    const { pedidoActual, mostrarFormulario, mostrarErrorFormulario , actualizarMostrarPedido, actualizarMostrarDetalle} = pedidosContext;

    const OnclickPedido = () =>{
        mostrarErrorFormulario(false);
        mostrarFormulario(false);
        actualizarMostrarDetalle(false);
        actualizarMostrarPedido(true);
        pedidoActual(pedido);
    }
    return ( 
        <li>
            <button
                type = "button"
                className = "btn btn-blank"
                onClick = {() => OnclickPedido()}
            >{pedido.nroPedido}
            </button>
        </li>
     );
}
 
export default Pedido;