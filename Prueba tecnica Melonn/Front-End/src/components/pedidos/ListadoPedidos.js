import React, {useContext, useEffect} from 'react';
import Pedido from './Pedido'
import pedidoContext from '../../context/pedidos/pedidoContext'

const ListadoPedidos = () => {

    //Extraemos los pedidos.
    const pedidosContext = useContext(pedidoContext);
    const { pedidos, obtenerPedidos, listaPedidos, actualizarListaPedidos } = pedidosContext;

    //Obtener pedidos al cargar.
    useEffect(() => {
        if(listaPedidos){
            obtenerPedidos();
            //2 seg de espacio para la actualizacion de la lista pedidos.
            setTimeout(function() {
                actualizarListaPedidos(false);
            }, 2000)
        }
    })

    //Condicionamos la visualizacion de pedidos.
    if(pedidos.length === 0) return null;

   
    return ( 
        <ul className = "listado-pedidos">
            {
                pedidos.map(pedido => (
                    <Pedido
                        key = {pedido.id}
                        pedido = {pedido}
                    />
                ))
            }
        </ul>
     );
}
 
export default ListadoPedidos;