import React from 'react';

const Producto = ({producto, pedido, modificarPedidoState}) => {
    const {id, nombreProducto, cantidadProducto, pesoProducto} = producto;

    const {listaProductos} = pedido;

    const eliminarProducto = (e, id) =>{
        e.preventDefault();
        const productos = listaProductos.filter(producto => producto.id !== id);
        modificarPedidoState({
            ...pedido,
            listaProductos: productos
        })
    }

    return ( 
        <div className = "contenedor-producto-individual sombra-dark">
            {
                (modificarPedidoState !== undefined) ?
                    <div className = "cerrar">
                        <button onClick = {(e) => eliminarProducto(e, id)}>X</button>
                    </div>
                    :
                    null
            }
            <div className = "campo-form-producto">
                <label>Nombre producto: </label>
                <label>{nombreProducto}</label>
            </div>
            <div className = "campo-form-producto">
                <label>Cantidad producto: </label>
                <label>{cantidadProducto}</label>
            </div>
            <div className = "campo-form-producto">
                <label>Peso producto: </label>
                <label>{pesoProducto}</label>
            </div>
        </div>
     );
}
 
export default Producto;