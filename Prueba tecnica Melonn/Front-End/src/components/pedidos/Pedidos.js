import React, {useContext} from 'react';
import Sidebar from '../layout/Sidebar'
import Barra from '../layout/Barra'
import FormPedido from './FormPedido'
import ResumenPedido from './ResumenPedido'
import DetallePedido from './DetallePedido'
import pedidoContext from '../../context/pedidos/pedidoContext'

const Pedidos = () => {

    const pedidosContext = useContext(pedidoContext);
    const { formulario, mostrarPedido, mostrarDetalle } = pedidosContext;
    return ( 
        <div className = "contenedor-app">
            <Sidebar />

            <div className = "seccion-principal">
                <Barra />
                <main>
                    {
                        formulario ?
                            <FormPedido/>
                            :
                            null
                    }
                    {
                        mostrarPedido ?
                            <ResumenPedido/>
                            :
                            null
                    }
                    {
                        mostrarDetalle ?
                            <DetallePedido/>
                            :
                            null
                    }
                </main>
            </div>

        </div>
     );
}
 
export default Pedidos;