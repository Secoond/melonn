import React , {useState, useEffect, useContext} from 'react';
import axios from 'axios'
import {v4 as uuidv4} from 'uuid';

import pedidoContext from '../../context/pedidos/pedidoContext'

import Producto from './Producto'


const FormPedido = () => {
    //Extraemos la funcion para agregar pedidos.
    const pedidosContext = useContext(pedidoContext);
    const { actualizarListaPedidos, errorFormulario, mostrarErrorFormulario, formularioProducto, mostrarFormularioProducto } = pedidosContext;

    //State para contener los metodos de envio.
    const [listaMetodosState, modificarListaMetodosState] = useState({metodos: []});

    const {metodos} = listaMetodosState;


    //State para el pedido.
    const [pedidoState, modificarPedidoState] = useState({
        vendedor : '',
        nroPedido: '',
        nombreComprador: '',
        telefonoComprador: '',
        emailComprador: '',
        direccionEnvio: '',
        ciudadEnvio: '',
        regionEnvio: '',
        paisEnvio: '',
        listaProductos: []
    })

    //Extraer pedido
    const {vendedor , nroPedido, nombreComprador, telefonoComprador, emailComprador, direccionEnvio, ciudadEnvio, regionEnvio, paisEnvio, listaProductos} = pedidoState;

    const [productoState, modificarProductoState] = useState({
        nombreProducto: '',
        cantidadProducto: '',
        pesoProducto: ''
    })

    const {nombreProducto, cantidadProducto, pesoProducto} = productoState;


    const crearPedido = (pedido) =>{
        axios.post("http://localhost:4000/api/Pedidos/CrearPedido", {
            pedido
        }, {
        }).then(response => {
            console.log(response);
        }).catch(function(err) {
            console.log(err);
        });
    }
    const onChange = e => {
        modificarPedidoState({
            ...pedidoState,
            [e.target.name]: e.target.value
        })
    }

    const onChangeProducto = e => {
        modificarProductoState({
            ...productoState,
            [e.target.name]: e.target.value
        })
    }

    const comprobarPedidoCampos = () =>{
        for(let element in pedidoState){
            if(pedidoState[element] === '')
                return true; 
        }

        return false;
    }

    const comprobarProductoCampos = () =>{
        for(let element in productoState){
            if(productoState[element] === '')
                return true; 
        }

        return false;
    }

    const onSubmit = e => {
        e.preventDefault();

        //Se valida campos vacios.
        if(comprobarPedidoCampos() || pedidoState.listaProductos.length === 0){
            mostrarErrorFormulario(true);
            return;
        }
        //Se pasa al action
        pedidoState.id = uuidv4();
        crearPedido(pedidoState);
        actualizarListaPedidos(true);
        //agregarPedido(pedidoState);
        modificarPedidoState({
            vendedor : '',
            metodoEnvio: '',
            nroPedido: '',
            nombreComprador: '',
            telefonoComprador: '',
            emailComprador: '',
            direccionEnvio: '',
            ciudadEnvio: '',
            regionEnvio: '',
            paisEnvio: '',
            listaProductos: []
        })
        //mostrarFormulario(false);
    }

    const agregarProducto = e =>{
        e.preventDefault();
        //Se valida campos vacios.
        if(comprobarProductoCampos()){
            mostrarErrorFormulario(true);
            return;
        }
        productoState.id = uuidv4();
        modificarPedidoState({
            ...pedidoState,
            listaProductos: [...listaProductos, productoState]
        })
        modificarProductoState({
            nombreProducto: '',
            cantidadProducto: '',
            pesoProducto: ''
        })
        mostrarFormularioProducto(false);

    }

    const visualizarFormularioProducto = e =>{
        e.preventDefault();
        mostrarFormularioProducto(true);
    }
    useEffect(() =>{
        if(metodos.length === 0){
            axios.get("http://localhost:4000/api/Pedidos/ListaMetodos", {
            }, {
            }).then(response => {
                modificarListaMetodosState({metodos: response.data.body});
            }).catch(function(err) {
                console.log(err);
            });
        }
    })

    return ( 
        <div className = "formulario">
            <div className = "contenedor-form sombra-dark">
                <h1>Crea un pedido</h1>
                { errorFormulario ? <p className = "mensaje error">Recuerda los campos obligatorios</p> : null}
                <form
                        onSubmit = {onSubmit}
                    >
                        <div className = "campo-form">
                            <label htmlFor = "vendedor">Vendedor*</label>
                            <input
                                type = "text"
                                id = "vendedor"
                                name = "vendedor"
                                value = {vendedor}
                                placeholder = "Vendedor"
                                onChange = {onChange}></input>
                        </div>
                        <div className = "campo-form">
                            <label htmlFor = "metotoEnvio">Metodos de Envio*</label>
                            <select name="metodoEnvio" id="metodoEnvio" onChange = {onChange} required>
                                <option hidden defaultValue>Seleccione su metodo de envío</option>
                                {
                                    metodos.map(metodo =>(
                                        <option key = {metodo.id} value = {metodo.id}>{metodo.name}</option>
                                    ))
                                }
                            </select>
                        </div>
                        <div className = "campo-form">
                            <label htmlFor = "nroPedido">Numero de pedido*</label>
                            <input
                                type = "text"
                                id = "nroPedido"
                                name = "nroPedido"
                                value = {nroPedido}
                                placeholder = "Numero de pedido"
                                onChange = {onChange}></input>
                        </div>

                        <div className = "campo-form">
                            <label htmlFor = "nombreComprador">Nombre del comprador*</label>
                            <input
                                type = "text"
                                id = "nombreComprador"
                                name = "nombreComprador"
                                value = {nombreComprador}
                                placeholder = "Nombre del comprador"
                                onChange = {onChange}></input>
                        </div>

                        <div className = "campo-form">
                            <label htmlFor = "telefonoComprador">Telefono del comprador*</label>
                            <input
                                type = "text"
                                id = "telefonoComprador"
                                name = "telefonoComprador"
                                value = {telefonoComprador}
                                placeholder = "Telefono del comprador"
                                onChange = {onChange}></input>
                        </div>

                        <div className = "campo-form">
                            <label htmlFor = "emailComprador">Email del comprador*</label>
                            <input
                                type = "email"
                                id = "emailComprador"
                                name = "emailComprador"
                                value = {emailComprador}
                                placeholder = "Email del comprador"
                                onChange = {onChange}></input>
                        </div>
                        
                        <div className = "campo-form">
                            <label htmlFor = "direccionEnvio">Direccion de envío*</label>
                            <input
                                type = "text"
                                id = "direccionEnvio"
                                name = "direccionEnvio"
                                value = {direccionEnvio}
                                placeholder = "Direccion de envío"
                                onChange = {onChange}></input>
                        </div>
                        

                        <div className = "campo-form">
                            <label htmlFor = "ciudadEnvio">Ciudad de envío*</label>
                            <input
                                type = "text"
                                id = "ciudadEnvio"
                                name = "ciudadEnvio"
                                value = {ciudadEnvio}
                                placeholder = "Ciudad de envío"
                                onChange = {onChange}></input>
                        </div>

                        <div className = "campo-form">
                            <label htmlFor = "regionEnvio">Region de envío*</label>
                            <input
                                type = "text"
                                id = "regionEnvio"
                                name = "regionEnvio"
                                value = {regionEnvio}
                                placeholder = "Region de envío"
                                onChange = {onChange}></input>
                        </div>

                        <div className = "campo-form">
                            <label htmlFor = "paisEnvio">Pais de envío*</label>
                            <input
                                type = "text"
                                id = "paisEnvio"
                                name = "paisEnvio"
                                value = {paisEnvio}
                                placeholder = "Pais de envío"
                                onChange = {onChange}></input>
                        </div>
                        <div className = "campo-form">
                            <label htmlFor = "listaProductos">Lista productos</label>
                            <button className = "btn btn-primario btn-block" onClick = {(e) => visualizarFormularioProducto(e)}>Agregar producto</button>
                        </div>
                        {
                            (formularioProducto) ?
                                <div className = "formulario-producto">
                                    <div className = "contenedor-producto sombra-dark">
                                        <div className = "campo-form-producto">
                                            <label htmlFor = "nombreProducto">Nombre producto</label>
                                            <input
                                                type = "text"
                                                id = "nombreProducto"
                                                name = "nombreProducto"
                                                value = {nombreProducto}
                                                placeholder = "Nombre"
                                                onChange = {onChangeProducto}></input>
                                        </div>
                                        <div className = "campo-form-producto">
                                            <label htmlFor = "cantidadProducto">Cantidad de productos</label>
                                            <input
                                                type = "text"
                                                id = "cantidadProducto"
                                                name = "cantidadProducto"
                                                value = {cantidadProducto}
                                                placeholder = "Cantidad"
                                                onChange = {onChangeProducto}></input>
                                        </div>
                                        <div className = "campo-form-producto">
                                            <label htmlFor = "pesoProducto">Peso del Producto</label>
                                            <input
                                                type = "text"
                                                id = "pesoProducto"
                                                name = "pesoProducto"
                                                value = {pesoProducto}
                                                placeholder = "Peso"
                                                onChange = {onChangeProducto}></input>
                                        </div>
                                        <div className = "campo-form">
                                            <button className = "btn btn-primario btn-block" onClick = {(e) => agregarProducto(e)}>Agregar</button>
                                        </div>
                                    </div>
                                </div>
                                
                                :
                                null
                        }
                        <div className = "campo-form">
                            {
                                listaProductos.map(producto =>(
                                    <Producto 
                                        key = {producto.id}
                                        producto = {producto}
                                        pedido =  {pedidoState}
                                        modificarPedidoState = {modificarPedidoState}
                                    />
                                ))
                            }
                        </div>
                        <div className = "campo-form">
                            <input type = "submit" className = "btn btn-primario btn-block" value = "Crear pedido" />
                        </div>
                </form>
            </div>
        </div>
     );
}
 
export default FormPedido;