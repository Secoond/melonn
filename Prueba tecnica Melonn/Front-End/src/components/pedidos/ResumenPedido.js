import React, {useEffect, useContext, useState} from 'react';
import pedidoContext from '../../context/pedidos/pedidoContext'
import axios from 'axios'
const ResumenPedido = () => {

    const pedidosContext = useContext(pedidoContext);
    const { pedido, actualizarMostrarPedido,  actualizarMostrarDetalle} = pedidosContext;

    //State para contener los metodos de envio.
    const [listaMetodosState, modificarListaMetodosState] = useState({metodos: []});

    const {metodos} = listaMetodosState;
    
    const obtenerNombreMetodo = id =>{
        for(let i = 0; i < metodos.length; i++){
            if(metodos[i].id === parseInt(id))
                return metodos[i].name;
        }
        return null;
    }

    const dateFormat = (fecha) =>
    {
        let ms = Date.parse(fecha);
        let fechaNew = new Date(ms);
        return fechaNew.toString();
    }

    const verDetallePedido = e =>{
        actualizarMostrarPedido(false);
        actualizarMostrarDetalle(true);
    }

    useEffect(() =>{
        if(metodos.length === 0){
            axios.get("http://localhost:4000/api/Pedidos/ListaMetodos", {
            }, {
            }).then(response => {
                modificarListaMetodosState({metodos: response.data.body});
            }).catch(function(err) {
                console.log(err);
            });
        }
    })
    return ( 
        <div className = "contenedor-form sombra-dark">
            <h2>Información del pedido</h2>
            <div className = "campo-form-pedido">
                <label className = "titulo">Numero de orden de venta: </label>
                <label>{pedido.nroPedido}</label>
            </div>
            <div className = "campo-form-pedido">
                <label className = "titulo">Vendedor: </label>
                <label>{pedido.vendedor}</label>
            </div>
            <div className = "campo-form-pedido">
                <label className = "titulo">fecha de creacion: </label>
                <label>{dateFormat(pedido.fechaCreacion)}</label>
            </div>
            <div className = "campo-form-pedido">
                <label className = "titulo">Metodo de envío: </label>
                <label>{obtenerNombreMetodo(pedido.metodoEnvio)}</label>
            </div>
            <div className = "campo-form">
                <button className = "btn btn-primario btn-block" onClick = {(e) => verDetallePedido(e)}>Ver detalle</button>
            </div>
        </div>
     );
}
 
export default ResumenPedido;