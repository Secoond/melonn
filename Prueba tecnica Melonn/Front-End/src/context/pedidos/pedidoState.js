import React, {useReducer} from 'react';
import axios from 'axios'
import pedidoContext from './pedidoContext'
import pedidoReducer from './pedidoReducer'
import { 
        FORMULARIO_PEDIDO,
        OBTENER_PEDIDOS, 
        AGREGAR_PEDIDO,
        PEDIDO_ACTUAL,
        MOSTRAR_PEDIDO,
        MOSTRAR_DETALLE,
        MOSTRAR_ERROR,
        FORMULARIO_PRODUCTO,
        LISTA_PEDIDOS} from '../../types'
        



const PedidoState = props => {


    const initialState = {
        pedidos : [],
        formulario: false,
        pedido: null,
        errorFormulario: false,
        formularioProducto: false,
        listaPedidos: true,
        mostrarPedido: false,
        mostrarDetalle: false
    }

    //Dispatch para ejecutar las acciones

    const [state, dispatch] = useReducer(pedidoReducer, initialState);

    //Serie de funciones para el CRUD
    const mostrarFormulario = estado  => {
        dispatch({
            type: FORMULARIO_PEDIDO,
            payload: estado
        })
    }

    const obtenerPedidos = async () => {
        try{
            const response = await axios.get("http://localhost:4000/api/Pedidos/ObtenerPedidos");
            dispatch({
                type: OBTENER_PEDIDOS,
                payload: response.data.response
            })
        }catch(error){
            console.log(error)
        }
    }

    const agregarPedido = pedido =>{
        //Inserta el pedido nuevo
        dispatch({
            type: AGREGAR_PEDIDO,
            payload: pedido
        })
    }

    const pedidoActual = pedido => {
        dispatch({
            type: PEDIDO_ACTUAL,
            payload: pedido
        })
    }

    const mostrarErrorFormulario = estado => {
        dispatch({
            type: MOSTRAR_ERROR,
            payload: estado
        })
    }
    const mostrarFormularioProducto = estado => {
        dispatch({
            type: FORMULARIO_PRODUCTO,
            payload: estado
        })
    }

    const actualizarListaPedidos = estado =>{
        dispatch({
            type: LISTA_PEDIDOS,
            payload: estado
        })
    }

    const actualizarMostrarPedido = estado =>{
        dispatch({
            type: MOSTRAR_PEDIDO,
            payload: estado
        })
    }

    const actualizarMostrarDetalle = estado =>{
        dispatch({
            type: MOSTRAR_DETALLE,
            payload: estado
        })
    }
    return (
        <pedidoContext.Provider
            value = {{
                pedidos: state.pedidos,
                formulario: state.formulario,
                pedido: state.pedido,
                errorFormulario: state.errorFormulario,
                formularioProducto: state.formularioProducto,
                listaPedidos: state.listaPedidos,
                mostrarPedido: state.mostrarPedido,
                mostrarDetalle: state.mostrarDetalle,
                mostrarFormulario,
                obtenerPedidos,
                agregarPedido,
                pedidoActual,
                mostrarErrorFormulario,
                mostrarFormularioProducto,
                actualizarListaPedidos,
                actualizarMostrarPedido,
                actualizarMostrarDetalle
            }}
        >
            {props.children}
        </pedidoContext.Provider>
    )
}

export default PedidoState;
