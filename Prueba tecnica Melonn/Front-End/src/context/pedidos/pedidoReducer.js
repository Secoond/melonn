import { 
    FORMULARIO_PEDIDO,
    OBTENER_PEDIDOS, 
    AGREGAR_PEDIDO,
    PEDIDO_ACTUAL,
    MOSTRAR_PEDIDO,
    MOSTRAR_DETALLE,
    MOSTRAR_ERROR,
    FORMULARIO_PRODUCTO,
    LISTA_PEDIDOS} from '../../types'

export default  (state, action) => {
    switch(action.type) {
        case FORMULARIO_PEDIDO:
            return {
                ...state,
                formulario: action.payload
            }
        case OBTENER_PEDIDOS:
            return {
                ...state,
                pedidos: action.payload
            }
        case AGREGAR_PEDIDO:
            return {
                ...state,
                pedidos: [...state.pedidos, action.payload]
            }
        case PEDIDO_ACTUAL:
            return {
                ...state,
                pedido: action.payload
            }
        case MOSTRAR_ERROR:
            return {
                ...state,
                errorFormulario: action.payload
            }
        case FORMULARIO_PRODUCTO:
            return {
                ...state,
                formularioProducto: action.payload
            }
        case LISTA_PEDIDOS:
            return {
                ...state,
                listaPedidos: action.payload
            }
        case MOSTRAR_PEDIDO:
            return {
                ...state,
                mostrarPedido: action.payload
            }
        case MOSTRAR_DETALLE:
            return {
                ...state,
                mostrarDetalle: action.payload
            }
        default: 
            return state;
    }
}