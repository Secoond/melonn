import React from 'react';
import Pedidos from './components/pedidos/Pedidos';

import PedidoState from './context/pedidos/pedidoState';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
function App() {
  return (
    <PedidoState>
      <Router>
          <Switch>
            <Route exact path = "/" component = {Pedidos} />
          </Switch>
      </Router>
   </PedidoState>
  );
}

export default App;
