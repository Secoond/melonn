"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.crearPedido = exports.obtenerPedidos = exports.obtenerListaMetodos = void 0;
const request_promise_1 = __importDefault(require("request-promise"));
const store_1 = __importDefault(require("store"));
const obtenerListaMetodos = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //Preparo el request con la url para la peticion.
    request_promise_1.default({
        uri: "https://yhua9e1l30.execute-api.us-east-1.amazonaws.com/sandbox/shipping-methods",
        headers: { "x-api-key": "oNhW2TBOlI1t4kWb3PEad1K1S1KxKuuI3GX6rGvT" },
        method: 'GET',
        json: true,
    }, function (error, response, body) {
        if (!error) {
            res.status(200).json(response);
        }
        else {
            res.status(400).json(error);
        }
    });
});
exports.obtenerListaMetodos = obtenerListaMetodos;
function obtenerDetallesMetodoEnvioInterno(shipping_method_id, callback) {
    return __awaiter(this, void 0, void 0, function* () {
        //Preparo el request con la url para la peticion.
        try {
            yield request_promise_1.default({
                uri: "https://yhua9e1l30.execute-api.us-east-1.amazonaws.com/sandbox/shipping-methods/" + shipping_method_id,
                headers: { "x-api-key": "oNhW2TBOlI1t4kWb3PEad1K1S1KxKuuI3GX6rGvT" },
                method: 'GET',
                json: true,
            }, function (error, response) {
                if (!error) {
                    callback({ statusCode: 200, response: response });
                }
                else {
                    callback({ statusCode: 400, error: error });
                }
            });
        }
        catch (error) {
            return { Exito: false, Mensaje: error };
        }
    });
}
function obtenerDiasLibresInterno(callback) {
    return __awaiter(this, void 0, void 0, function* () {
        //Preparo el request con la url para la peticion.
        try {
            yield request_promise_1.default({
                uri: "https://yhua9e1l30.execute-api.us-east-1.amazonaws.com/sandbox/off-days",
                headers: { "x-api-key": "oNhW2TBOlI1t4kWb3PEad1K1S1KxKuuI3GX6rGvT" },
                method: 'GET',
                json: true,
            }, function (error, response) {
                if (!error) {
                    callback({ statusCode: 200, response: response });
                }
                else {
                    callback({ statusCode: 400, error: error });
                }
            });
        }
        catch (error) {
            return { Exito: false, Mensaje: error };
        }
    });
}
const obtenerPedidos = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //Obtengo la lista de pedidos.
    let pedidos = store_1.default.get('pedidos');
    if (pedidos == undefined || pedidos == null)
        pedidos = [];
    res.status(200).json({ Exito: true, response: pedidos });
});
exports.obtenerPedidos = obtenerPedidos;
const crearPedido = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //Obtenemos el JSON de la peticion POST
    let bodyInterno = req.body.pedido;
    //Establecemos la fecha actual y la convertimos al formato correspondiente guardandola en el JSON.
    const fecha = new Date();
    bodyInterno.fechaCreacion = fecha.toISOString();
    //Convertimos la fecha actual a epoch y obtenemos un numero aleatorio entre 0 y 100.
    bodyInterno.internalId = Math.floor(fecha.getTime() / 1000.0) + Math.floor(Math.random() * (100 + 1));
    //Definimos la variable para el fallo de promesas
    let promesasNull = false;
    //Definimos las variables de promesas.
    let pack_promise_min;
    let pack_promise_max;
    let ship_promise_min;
    let ship_promise_max;
    let delivery_promise_min;
    let delivery_promise_max;
    let ready_pickup_promise_min;
    let ready_pickup_promise_max;
    //Obtenemos los detalles del metodo de envio especificado en el pedido y tambien los dias libres.
    yield obtenerDetallesMetodoEnvioInterno(bodyInterno.metodoEnvio, function (dataDetalles) {
        return __awaiter(this, void 0, void 0, function* () {
            if (dataDetalles.statusCode != 400) {
                const reglasEnvio = dataDetalles.response.body;
                yield obtenerDiasLibresInterno(function (dataDias) {
                    return __awaiter(this, void 0, void 0, function* () {
                        if (dataDias.statusCode != 400) {
                            const diasLibres = dataDias.response.body;
                            //Determina si el dia actual es dia de trabajo
                            const diaActualIsABusinessDay = comprobarDiaTrabajo(diasLibres, fecha);
                            //constuyo un array de 10 dias de trabajo a partir de el dia actual.
                            const fechaSiguiente = fecha;
                            fechaSiguiente.setDate(fechaSiguiente.getDate() + 1);
                            const nextBusinessDays = calcularDiasDeTrabajo(diasLibres, fechaSiguiente);
                            //Compruebo si añadieron rpoductos al pedido.
                            if (bodyInterno.listaProductos.length == 0) {
                                promesasNull = true;
                            }
                            else {
                                //Validate: Weight
                                const minWeight = reglasEnvio.rules.availability.byWeight.min;
                                const maxWeight = reglasEnvio.rules.availability.byWeight.max;
                                for (let i = 0; i < bodyInterno.listaProductos.length; i++) {
                                    if (parseInt(bodyInterno.listaProductos[i].pesoProducto) > maxWeight ||
                                        parseInt(bodyInterno.listaProductos[i].pesoProducto) < minWeight) {
                                        promesasNull = true;
                                    }
                                }
                                if (!promesasNull) {
                                    //Validate based on request time availability.
                                    const dayType = reglasEnvio.rules.availability.byRequestTime.dayType;
                                    const fromTimeOfDay = reglasEnvio.rules.availability.byRequestTime.fromTimeOfDay;
                                    const toTimeOfDay = reglasEnvio.rules.availability.byRequestTime.toTimeOfDay;
                                    if (dayType == "BUSINESS" && !diaActualIsABusinessDay) {
                                        promesasNull = true;
                                    }
                                    else {
                                        const horaActual = fecha.getHours();
                                        if (horaActual > toTimeOfDay || horaActual < fromTimeOfDay) {
                                            promesasNull = true;
                                        }
                                        else {
                                            //Calculate promises
                                            const casos = reglasEnvio.rules.promisesParameters.cases;
                                            let priority = 1;
                                            let workingCase;
                                            //Determine wich case applies
                                            while (!promesasNull && workingCase == null) {
                                                let casoARevisar;
                                                for (let i = 0; i < casos.length; i++) {
                                                    if (casos[i].priority = priority)
                                                        casoARevisar = casos[i];
                                                }
                                                if (casoARevisar.priority != priority) {
                                                    promesasNull = true;
                                                }
                                                else {
                                                    const dayType = casoARevisar.condition.byRequestTime.dayType;
                                                    const fromTimeOfDay = casoARevisar.condition.byRequestTime.fromTimeOfDay;
                                                    const toTimeOfDay = casoARevisar.condition.byRequestTime.toTimeOfDay;
                                                    if (dayType == "BUSINESS" && !diaActualIsABusinessDay)
                                                        priority++;
                                                    else if (horaActual > toTimeOfDay || horaActual < fromTimeOfDay)
                                                        priority++;
                                                    else
                                                        workingCase = casoARevisar;
                                                }
                                            }
                                            let fechaGMT_O = obtenerFechaGMT();
                                            //Calculate PackPromise
                                            let minType = workingCase.packPromise.min.type;
                                            let minDeltaHours = workingCase.packPromise.min.deltaHours;
                                            let minDeltaBusiness = workingCase.packPromise.min.deltaBusinessDays;
                                            let minTimeOfDay = workingCase.packPromise.min.timeOfDay;
                                            let maxType = workingCase.packPromise.max.type;
                                            let maxDeltaHours = workingCase.packPromise.max.deltaHours;
                                            let maxDeltaBusiness = workingCase.packPromise.max.deltaBusinessDays;
                                            let maxTimeOfDay = workingCase.packPromise.max.timeOfDay;
                                            if (minType == "NULL")
                                                pack_promise_min = null;
                                            else if (minType == "DELTA-HOURS") {
                                                const fechaPackPromiseMin = fechaGMT_O;
                                                fechaPackPromiseMin.setHours(fechaPackPromiseMin.getHours() + minDeltaHours);
                                                pack_promise_min = fechaPackPromiseMin.toDateString();
                                            }
                                            else {
                                                const fechaSeparada = nextBusinessDays[(minDeltaBusiness - 1) < 0 ? 0 : minDeltaBusiness - 1].split("-");
                                                const nuevaFecha = new Date(parseInt(fechaSeparada[0]), parseInt(fechaSeparada[1]) - 1, parseInt(fechaSeparada[2]), minTimeOfDay, 0, 0);
                                                pack_promise_min = new Date(nuevaFecha.getUTCFullYear(), nuevaFecha.getUTCMonth(), nuevaFecha.getUTCDate(), nuevaFecha.getUTCHours(), nuevaFecha.getUTCMinutes(), nuevaFecha.getUTCSeconds()).toDateString();
                                            }
                                            if (maxType == "NULL")
                                                pack_promise_max = null;
                                            else if (maxType == "DELTA-HOURS") {
                                                const fechaPackPromiseMax = fechaGMT_O;
                                                fechaPackPromiseMax.setHours(fechaPackPromiseMax.getHours() + maxDeltaHours);
                                                pack_promise_max = fechaPackPromiseMax.toDateString();
                                            }
                                            else {
                                                const fechaSeparada = nextBusinessDays[(maxDeltaBusiness - 1) < 0 ? 0 : maxDeltaBusiness - 1].split("-");
                                                const nuevaFecha = new Date(parseInt(fechaSeparada[0]), parseInt(fechaSeparada[1]) - 1, parseInt(fechaSeparada[2]), maxTimeOfDay, 0, 0);
                                                pack_promise_max = new Date(nuevaFecha.getUTCFullYear(), nuevaFecha.getUTCMonth(), nuevaFecha.getUTCDate(), nuevaFecha.getUTCHours(), nuevaFecha.getUTCMinutes(), nuevaFecha.getUTCSeconds()).toDateString();
                                            }
                                            //Calculate ShipPromise
                                            minType = workingCase.shipPromise.min.type;
                                            minDeltaHours = workingCase.shipPromise.min.deltaHours;
                                            minDeltaBusiness = workingCase.shipPromise.min.deltaBusinessDays;
                                            minTimeOfDay = workingCase.shipPromise.min.timeOfDay;
                                            maxType = workingCase.shipPromise.max.type;
                                            maxDeltaHours = workingCase.shipPromise.max.deltaHours;
                                            maxDeltaBusiness = workingCase.shipPromise.max.deltaBusinessDays;
                                            maxTimeOfDay = workingCase.shipPromise.max.timeOfDay;
                                            if (minType == "NULL")
                                                ship_promise_min = null;
                                            else if (minType == "DELTA-HOURS") {
                                                const fechaShipPromiseMin = fechaGMT_O;
                                                fechaShipPromiseMin.setHours(fechaShipPromiseMin.getHours() + minDeltaHours);
                                                ship_promise_min = fechaShipPromiseMin.toDateString();
                                            }
                                            else {
                                                const fechaSeparada = nextBusinessDays[(minDeltaBusiness - 1) < 0 ? 0 : minDeltaBusiness - 1].split("-");
                                                const nuevaFecha = new Date(parseInt(fechaSeparada[0]), parseInt(fechaSeparada[1]) - 1, parseInt(fechaSeparada[2]), minTimeOfDay, 0, 0);
                                                ship_promise_min = new Date(nuevaFecha.getUTCFullYear(), nuevaFecha.getUTCMonth(), nuevaFecha.getUTCDate(), nuevaFecha.getUTCHours(), nuevaFecha.getUTCMinutes(), nuevaFecha.getUTCSeconds()).toDateString();
                                            }
                                            if (maxType == "NULL")
                                                ship_promise_max = null;
                                            else if (maxType == "DELTA-HOURS") {
                                                const fechaShipPromiseMax = fechaGMT_O;
                                                fechaShipPromiseMax.setHours(fechaShipPromiseMax.getHours() + minDeltaHours);
                                                ship_promise_max = fechaShipPromiseMax.toDateString();
                                            }
                                            else {
                                                const fechaSeparada = nextBusinessDays[(maxDeltaBusiness - 1) < 0 ? 0 : maxDeltaBusiness - 1].split("-");
                                                const nuevaFecha = new Date(parseInt(fechaSeparada[0]), parseInt(fechaSeparada[1]) - 1, parseInt(fechaSeparada[2]), maxTimeOfDay, 0, 0);
                                                ship_promise_max = new Date(nuevaFecha.getUTCFullYear(), nuevaFecha.getUTCMonth(), nuevaFecha.getUTCDate(), nuevaFecha.getUTCHours(), nuevaFecha.getUTCMinutes(), nuevaFecha.getUTCSeconds()).toDateString();
                                            }
                                            //Calculate DeliveryPromise
                                            minType = workingCase.deliveryPromise.min.type;
                                            minDeltaHours = workingCase.deliveryPromise.min.deltaHours;
                                            minDeltaBusiness = workingCase.deliveryPromise.min.deltaBusinessDays;
                                            minTimeOfDay = workingCase.deliveryPromise.min.timeOfDay;
                                            maxType = workingCase.deliveryPromise.max.type;
                                            maxDeltaHours = workingCase.deliveryPromise.max.deltaHours;
                                            maxDeltaBusiness = workingCase.deliveryPromise.max.deltaBusinessDays;
                                            maxTimeOfDay = workingCase.deliveryPromise.max.timeOfDay;
                                            if (minType == "NULL")
                                                delivery_promise_min = null;
                                            else if (minType == "DELTA-HOURS") {
                                                const fechaDeliveryPromiseMin = fechaGMT_O;
                                                fechaDeliveryPromiseMin.setHours(fechaDeliveryPromiseMin.getHours() + minDeltaHours);
                                                delivery_promise_min = fechaDeliveryPromiseMin.toDateString();
                                            }
                                            else {
                                                const fechaSeparada = nextBusinessDays[(minDeltaBusiness - 1) < 0 ? 0 : minDeltaBusiness - 1].split("-");
                                                const nuevaFecha = new Date(parseInt(fechaSeparada[0]), parseInt(fechaSeparada[1]) - 1, parseInt(fechaSeparada[2]), minTimeOfDay, 0, 0);
                                                delivery_promise_min = new Date(nuevaFecha.getUTCFullYear(), nuevaFecha.getUTCMonth(), nuevaFecha.getUTCDate(), nuevaFecha.getUTCHours(), nuevaFecha.getUTCMinutes(), nuevaFecha.getUTCSeconds()).toDateString();
                                            }
                                            if (maxType == "NULL")
                                                delivery_promise_max = null;
                                            else if (maxType == "DELTA-HOURS") {
                                                const fechaDeliveryPromiseMax = fechaGMT_O;
                                                fechaDeliveryPromiseMax.setHours(fechaDeliveryPromiseMax.getHours() + maxDeltaHours);
                                                delivery_promise_max = fechaDeliveryPromiseMax.toDateString();
                                            }
                                            else {
                                                const fechaSeparada = nextBusinessDays[(maxDeltaBusiness - 1) < 0 ? 0 : maxDeltaBusiness - 1].split("-");
                                                const nuevaFecha = new Date(parseInt(fechaSeparada[0]), parseInt(fechaSeparada[1]) - 1, parseInt(fechaSeparada[2]), maxTimeOfDay, 0, 0);
                                                delivery_promise_max = new Date(nuevaFecha.getUTCFullYear(), nuevaFecha.getUTCMonth(), nuevaFecha.getUTCDate(), nuevaFecha.getUTCHours(), nuevaFecha.getUTCMinutes(), nuevaFecha.getUTCSeconds()).toDateString();
                                            }
                                            //Calculate ReadyPickUpPromise
                                            minType = workingCase.readyPickUpPromise.min.type;
                                            minDeltaHours = workingCase.readyPickUpPromise.min.deltaHours;
                                            minDeltaBusiness = workingCase.readyPickUpPromise.min.deltaBusinessDays;
                                            minTimeOfDay = workingCase.readyPickUpPromise.min.timeOfDay;
                                            maxType = workingCase.readyPickUpPromise.max.type;
                                            maxDeltaHours = workingCase.readyPickUpPromise.max.deltaHours;
                                            maxDeltaBusiness = workingCase.readyPickUpPromise.max.deltaBusinessDays;
                                            maxTimeOfDay = workingCase.readyPickUpPromise.max.timeOfDay;
                                            if (minType == "NULL")
                                                ready_pickup_promise_min = null;
                                            else if (minType == "DELTA-HOURS") {
                                                const fechaReadyPickupPromiseMin = fechaGMT_O;
                                                fechaReadyPickupPromiseMin.setHours(fechaReadyPickupPromiseMin.getHours() + minDeltaHours);
                                                ready_pickup_promise_min = fechaReadyPickupPromiseMin.toDateString();
                                            }
                                            else {
                                                const fechaSeparada = nextBusinessDays[(minDeltaBusiness - 1) < 0 ? 0 : minDeltaBusiness - 1].split("-");
                                                const nuevaFecha = new Date(parseInt(fechaSeparada[0]), parseInt(fechaSeparada[1]) - 1, parseInt(fechaSeparada[2]), minTimeOfDay, 0, 0);
                                                ready_pickup_promise_min = new Date(nuevaFecha.getUTCFullYear(), nuevaFecha.getUTCMonth(), nuevaFecha.getUTCDate(), nuevaFecha.getUTCHours(), nuevaFecha.getUTCMinutes(), nuevaFecha.getUTCSeconds()).toDateString();
                                            }
                                            if (maxType == "NULL")
                                                ready_pickup_promise_max = null;
                                            else if (maxType == "DELTA-HOURS") {
                                                const fechaReadyPickupPromiseMax = fechaGMT_O;
                                                fechaReadyPickupPromiseMax.setHours(fechaReadyPickupPromiseMax.getHours() + maxDeltaHours);
                                                ready_pickup_promise_max = fechaReadyPickupPromiseMax.toDateString();
                                            }
                                            else {
                                                const fechaSeparada = nextBusinessDays[(maxDeltaBusiness - 1) < 0 ? 0 : maxDeltaBusiness - 1].split("-");
                                                const nuevaFecha = new Date(parseInt(fechaSeparada[0]), parseInt(fechaSeparada[1]) - 1, parseInt(fechaSeparada[2]), maxTimeOfDay, 0, 0);
                                                ready_pickup_promise_max = new Date(nuevaFecha.getUTCFullYear(), nuevaFecha.getUTCMonth(), nuevaFecha.getUTCDate(), nuevaFecha.getUTCHours(), nuevaFecha.getUTCMinutes(), nuevaFecha.getUTCSeconds()).toDateString();
                                            }
                                        }
                                    }
                                }
                            }
                            if (promesasNull) {
                                pack_promise_min = null;
                                pack_promise_max = null;
                                ship_promise_min = null;
                                ship_promise_max = null;
                                delivery_promise_min = null;
                                delivery_promise_max = null;
                                ready_pickup_promise_min = null;
                                ready_pickup_promise_max = null;
                            }
                            bodyInterno.pack_promise_min = pack_promise_min;
                            bodyInterno.pack_promise_max = pack_promise_max;
                            bodyInterno.ship_promise_min = ship_promise_min;
                            bodyInterno.ship_promise_max = ship_promise_max;
                            bodyInterno.delivery_promise_min = delivery_promise_min;
                            bodyInterno.delivery_promise_max = delivery_promise_max;
                            bodyInterno.ready_pickup_promise_min = ready_pickup_promise_min;
                            bodyInterno.ready_pickup_promise_max = ready_pickup_promise_max;
                            //Actualizo la lista de pedidos.
                            let pedidos = store_1.default.get('pedidos');
                            if (pedidos != undefined && pedidos != null)
                                pedidos.push(bodyInterno);
                            else
                                pedidos = [bodyInterno];
                            store_1.default.remove('pedidos');
                            store_1.default.set('pedidos', pedidos);
                        }
                        else {
                            res.status(400).json({ Exito: false, Mensaje: dataDias.error });
                        }
                    });
                });
            }
            else {
                res.status(400).json({ Exito: false, Mensaje: dataDetalles.error });
            }
        });
    });
    res.status(200).json({ Exito: true, Mensaje: "Se creado el pedido con exito!" });
});
exports.crearPedido = crearPedido;
function obtenerFechaGMT() {
    const fecha = new Date();
    return new Date(fecha.getUTCFullYear(), fecha.getUTCMonth(), fecha.getUTCDate(), fecha.getUTCHours(), fecha.getUTCMinutes(), fecha.getUTCSeconds());
}
function comprobarDiaTrabajo(diasLibres, fechaActual) {
    const ceroDelMes = ((fechaActual.getMonth() + 1) < 10) ? "-0" : "-";
    const ceroDelDia = (fechaActual.getDate() < 10) ? "-0" : "-";
    const fechaAComparar = fechaActual.getFullYear() + ceroDelMes + (fechaActual.getMonth() + 1) + ceroDelDia + fechaActual.getDate();
    for (let i = 0; i < diasLibres.length; i++) {
        if (fechaAComparar == diasLibres[i]) {
            return false;
        }
    }
    return true;
}
function calcularDiasDeTrabajo(diasLibres, fechaActual) {
    let listaDeDias = [];
    while (listaDeDias.length < 10) {
        const ceroDelMes = ((fechaActual.getMonth() + 1) < 10) ? "-0" : "-";
        const ceroDelDia = (fechaActual.getDate() < 10) ? "-0" : "-";
        const fechaAComparar = fechaActual.getFullYear() + ceroDelMes + (fechaActual.getMonth() + 1) + ceroDelDia + fechaActual.getDate();
        let entro = true;
        for (let i = 0; i < diasLibres.length; i++) {
            if (fechaAComparar == diasLibres[i]) {
                entro = false;
                break;
            }
        }
        if (entro)
            listaDeDias.push(fechaAComparar);
        fechaActual.setDate(fechaActual.getDate() + 1);
    }
    return listaDeDias;
}
