"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const pedidos_controller_1 = require("../controllers/pedidos.controller");
class PedidoRoute {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get('/ListaMetodos', pedidos_controller_1.obtenerListaMetodos),
            this.router.get('/ObtenerPedidos', pedidos_controller_1.obtenerPedidos),
            this.router.post('/CrearPedido', pedidos_controller_1.crearPedido);
    }
}
const PedidoR = new PedidoRoute();
exports.default = PedidoR.router;
