"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const helmet_1 = __importDefault(require("helmet"));
const cors_1 = __importDefault(require("cors"));
const path_1 = __importDefault(require("path"));
const pedidosRoute_1 = __importDefault(require("./routes/pedidosRoute"));
class Servidor {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    // Configuraciones de middlewares
    config() {
        // Puerto de escucha de la aplicacion
        this.app.set("port", process.env.PORT || 4000);
        // Segmento de configuracion de archivos estaticos
        this.app.use("/static", express_1.default.static(path_1.default.join(__dirname, "public")));
        // Configure Express to use EJS
        this.app.set("views", path_1.default.join(__dirname, "views"));
        this.app.set("view engine", "ejs");
        this.app.use(express_1.default.json()); // Implementacion de json en respuestas en los endpoints
        this.app.use(express_1.default.urlencoded({ extended: true }));
        this.app.use(helmet_1.default());
        this.app.use((req, res, next) => {
            //res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
            res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
            res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
            next();
        });
        this.http = require("http").Server(this.app);
        this.app.use(cors_1.default());
    }
    routes() {
        this.app.use("/api/TodoBien", (req, res) => res.send("Todo bien, todo correcto"));
        this.app.use("/api/Pedidos", pedidosRoute_1.default);
    }
    /**
   * Inicializacion del servidor
   */
    start() {
        this.http.listen(this.app.get("port"), () => {
            console.log(`Servidor iniciado en http://localhost:${this.app.get("port")}`);
        });
    }
}
const server = new Servidor();
server.start();
