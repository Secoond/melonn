import express, { json } from "express";
import helmet from "helmet";
import cors from "cors";
import path from "path";
import LocalStorage from "node-localstorage"

import { Request, Response} from 'express';
import Pedidos from "./routes/pedidosRoute";



class Servidor {
    public app: express.Application; // Varible que viene con el plugin de express
    public http: any; // Variable de peticiones http
  
    constructor() {
      this.app = express(); 
      this.config();
      this.routes();
    }
    
    // Configuraciones de middlewares
    config() {
      
      // Puerto de escucha de la aplicacion
      this.app.set("port", process.env.PORT || 4000);
      // Segmento de configuracion de archivos estaticos
      this.app.use("/static", express.static(path.join(__dirname, "public")));
      // Configure Express to use EJS
      this.app.set("views", path.join(__dirname, "views"));
      this.app.set("view engine", "ejs");
      
      this.app.use(express.json()); // Implementacion de json en respuestas en los endpoints
      this.app.use(express.urlencoded({ extended: true }));
      this.app.use(helmet());
      this.app.use((req: any, res: any, next: any) => {
        //res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
        res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
        next();
      })
      this.http = require("http").Server(this.app);
      
      this.app.use(cors());
    }

    routes() {
      this.app.use("/api/TodoBien", (req: Request, res: Response) => res.send("Todo bien, todo correcto"));
      this.app.use("/api/Pedidos", Pedidos);
    }

    /**
   * Inicializacion del servidor
   */
  start() {
    this.http.listen(this.app.get("port"), () => {
      console.log(
        `Servidor iniciado en http://localhost:${this.app.get("port")}`
      );
    });
  }
}

const server = new Servidor();
server.start();
 