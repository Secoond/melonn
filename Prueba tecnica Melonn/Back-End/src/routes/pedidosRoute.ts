import { Router } from 'express';

import {obtenerListaMetodos,  crearPedido, obtenerPedidos}  from '../controllers/pedidos.controller'



class PedidoRoute{
    router: Router;

    constructor(){
        this.router = Router();
        this.routes();
    }

    routes(){
        this.router.get('/ListaMetodos', obtenerListaMetodos),
        this.router.get('/ObtenerPedidos', obtenerPedidos),
        this.router.post('/CrearPedido', crearPedido)
        
    }
}

const PedidoR = new PedidoRoute()
export default PedidoR.router