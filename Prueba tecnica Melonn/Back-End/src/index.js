"use strict";
exports.__esModule = true;
var express_1 = require("express");
var helmet_1 = require("helmet");
var cors_1 = require("cors");
var path_1 = require("path");
var Servidor = /** @class */ (function () {
    function Servidor() {
        this.app = express_1["default"]();
        this.config();
        this.routes();
    }
    // Configuraciones de middlewares
    Servidor.prototype.config = function () {
        // Puerto de escucha de la aplicacion
        this.app.set("port", process.env.PORT || 4000);
        // Segmento de configuracion de archivos estaticos
        this.app.use("/static", express_1["default"].static(path_1["default"].join(__dirname, "public")));
        // Configure Express to use EJS
        this.app.set("views", path_1["default"].join(__dirname, "views"));
        this.app.set("view engine", "ejs");
        this.app.use(express_1["default"].json()); // Implementacion de json en respuestas en los endpoints
        this.app.use(express_1["default"].urlencoded({ extended: true }));
        this.app.use(helmet_1["default"]());
        this.app.use(function (req, res, next) {
            //res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
            res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
            res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
            next();
        });
        this.http = require("http").Server(this.app);
        this.app.use(cors_1["default"]());
        var io = require("socket.io")(this.http); // Inicializacion de Socketio
        io.origins("*:*");
        // Creacion de socketio como variable global
        this.app.set("socketio", io);
    };
    Servidor.prototype.routes = function () {
        this.app.use("/api/TodoBien", function (req, res) { return res.send("Todo bien, todo correcto"); });
    };
    /**
   * Inicializacion del servidor
   */
    Servidor.prototype.start = function () {
        var _this = this;
        this.http.listen(this.app.get("port"), function () {
            console.log("Servidor iniciado en http://localhost:" + _this.app.get("port"));
        });
    };
    return Servidor;
}());
var server = new Servidor();
server.start();
