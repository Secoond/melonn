# Instrucciones para inicicializacion del proyecto

1. Ejecutar `npm install`.
2. Ejecutar `npm run dev`.

Este ultimo comando ejecuta el modo escucha para la compilacion de archivos de TypeScript a JavaScript
y a la vez ejecuta el proyecto. (Revisar package.json)


Este proyecto esta desplegado en: https://melonn.herokuapp.com/